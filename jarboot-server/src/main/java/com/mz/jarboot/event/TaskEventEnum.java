package com.mz.jarboot.event;

/**
 * @author majianzheng
 */

public enum TaskEventEnum {
    /**
     * 重启
     */
    RESTART,

    /**
     * 自动启动
     */
    AUTO_START_ALL,

    /**
     * 服务离线事件
     */
    OFFLINE
}
